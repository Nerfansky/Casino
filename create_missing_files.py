import os

def create_missing_files():
    for filename in ["last_balance.txt", "Balance.txt", "Fatal_error.txt"]:
        if not os.path.exists(filename):
            with open(filename, "w") as f:
                f.write("")
