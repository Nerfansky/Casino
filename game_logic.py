import time
from statistics import mean
from get_Bet import get_Bet
from spin import spin
from create_missing_files import create_missing_files
from clear_screen import clear_screen

def game_logic(balance):
    try:
        Symbols = ["apple", "pear", "cherry", "orange", "jackpot", "zero"]
        Coefficient = {
            "apple": 1,
            "pear": 1.5,
            "zero": 0.1,
            "cherry": 5,
            "orange": 0.5,
            "jackpot": 10
        }

        chances = {
            "apple": 0.2,
            "pear": 0.1,
            "zero": 0.3,
            "cherry": 0.1,
            "orange": 0.2,
            "jackpot": 0.1
        }

        create_missing_files()

        try:
            with open("last_balance.txt", "r") as f:
                content = f.read()
                if content.strip():
                    balance = float(content)
                else:
                    while True:
                        initial_balance = input("how much dib you buy?  ")
                        if initial_balance.isdigit():
                            balance = float(initial_balance)
                            break
                        else:
                            print("Enter a valid number.")
        except FileNotFoundError:
            while True:
                initial_balance = input("how much dib you buy?  ")
                if initial_balance.isdigit():
                    balance = float(initial_balance)
                    break
                else:
                    print("Enter a valid number.")

        while True:
            bet = get_Bet(balance)
            spin_result = spin(chances)
            print("\nSpinning The Wheel\n")
            time.sleep(3)

            for _ in range(3):
                print("rolling\n")
                time.sleep(1)
            print("Spin Result:\n\n", spin_result, "\n")

            convertations = [Coefficient[word] for word in spin_result]
            avg = mean(convertations)
            average = (round(avg))
            win = average * bet
            balance += win
            print("Your winnings are", win, "$\n")
            print("Your current balance is", balance, "$\n")

            with open("last_balance.txt", "w") as f:
                f.write(str(balance))

            with open("Data_Base.txt", "a") as file:
                file.write(str(win) + "\n")

            if input("Continue playing (yes/no): ").lower() != "yes":
                clear_screen()
                break

    except KeyboardInterrupt:
        raise KeyboardInterrupt
    except Exception as e:
        print("An error occurred:", e)
        with open("Fatal_error.txt", "a") as f:
            f.write("An error occurred: " + str(e) + "\n")
